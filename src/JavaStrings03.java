public class JavaStrings03 {
    public static void main(String[] args) {
        String txt = "Hello World";
        System.out.println(txt.toUpperCase()); // Output "HELLO WORLD"
        System.err.println(txt.toLowerCase()); // Output "hello world"
    }
}
