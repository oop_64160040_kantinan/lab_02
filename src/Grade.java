import java.util.Scanner;

public class Grade {
    public static void main(String[] args) {
        String grade;
        int score;
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input your score:");
        grade = sc.next();
        score = Integer.parseInt(grade);
        if (score >= 80){
            System.err.println("Grade: A");
        }else if(score >= 75){
            System.out.println("Grade: B+");
        }else if(score >= 70){
            System.out.println("Grade: B");
        }else if(score >= 65){
            System.out.println("Grade: C+");
        }else if(score >= 60){
            System.out.println("Grade: C");
        }else if(score >= 55){
            System.out.println("Grade: D+");
        }else if(score >= 50){
            System.out.println("Grade: D");
        }else if(score <= 49){
            System.out.println("Grade: F");
        }
        
    }
}
