public class JavaVariables05 {
    public static void main(String[] args) {
        int myNum = 15;
        myNum = 20; // will generate an error: cannot assign a value to a final variable
        System.out.println(myNum);
    }
}
